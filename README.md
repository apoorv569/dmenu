# My dmenu build

dmenu is a dynamic menu for X, originally designed for dwm. dmenu is created by the good folks at [suckless.org](https://suckless.org).  This is my personal build of dmenu. I used a number of patches in this build. The patches I added to this build include:
+ border (for having border around dmenu)
+ center (for spawning dmenu center screen instead of top)
+ fuzzy-highlight (highlights the fuzzy matched characters)
+ fuzzy-match (support for fuzzy-matching for dmenu)
+ lineheight (allows to change dmenu height)
+ morecolor (creates an additional color scheme, for use with the entries adjacent to the selection)

# Dependencies
+ ttf-joypixels

Also, you will need to add the following from the AUR:
+ nerd-fonts-mononoki (or you can install nerd-fonts-complete for all the nerd fonts)
+ https://aur.archlinux.org/packages/libxft-bgra/ (needed for colored fonts and emojis)

# Installing

Download the source code from this repository or use a git clone:

	git clone https://gitlab.com/apoorv569/dmenu.git
	cd dmenu
    sudo make clean install
	
NOTE: Installing this will overwrite your existing dmenu installation.
